
/**
 * Initialize and start pjsua.
 *
 * @param sipUser the sip username to be used for register
 * @param sipDomain the domain of the sip register server
 *
 * @return When successful, returns 0.
 */

#ifndef __ZEMPLUS_CALL_H__
#define __ZEMPLUS_CALL_H__

#include <pjsua-lib/pjsua.h>
#import <AudioToolbox/AudioToolbox.h>

/* Pjsua application data */
typedef struct app_config_t
{
    pj_pool_t		   *pool;
    int           ringback_slot;
    int           ringback_cnt;
    pjmedia_port *ringback_port;
    int			    ring_slot;
    int			    ring_cnt;
    pjmedia_port	   *ring_port;
    SystemSoundID     ring_id;
    CFRunLoopTimerRef ring_timer;
    pj_bool_t		    ring_on;

} app_config_t;




pjsua_media_config media_cfg;


int startPjsip();
/**
 * Make VoIP call.
 *
 * @param destUri the uri of the receiver, something like "sip:192.168.43.106:5080"
 */
pj_status_t makeCall(char* destUri,pjsua_call_id *call_id,pjsua_call_setting call_opt);

/**
 * End ongoing VoIP calls
 */
void endCall();
void acceptcall(pjsua_call_id *callID);

int sip_Connect();
void restartPJSIP();

void sip_call_play_digit(pjsua_call_id call_id, char digit);
pj_status_t sip_disconnect();
void get_account_status();
void unholdCall(pjsua_call_id call_id,pjsua_call_setting call_opt);
void sip_set_registration();
void sip_set_unregistration();
#endif


