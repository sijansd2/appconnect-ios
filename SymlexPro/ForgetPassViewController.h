//
//  ForgetPassViewController.h
//  ShellVoice
//
//  Created by Sijan's Mac on 7/2/18.
//  Copyright © 2018 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgetPassViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end
