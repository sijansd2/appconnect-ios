//
//  CityChooserViewController.h
//  helloworld
//
//  Created by Riaz on 5/18/16.
//  Copyright © 2016 Riaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ComboDelegate;

@interface ComboView : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
@property (nonatomic, strong) NSString *countryCode;

@property (assign, nonatomic) id <ComboDelegate>delegate;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (nonatomic, assign) bool isFiltered;

@property(nonatomic,strong) NSString *comboType;
@property(nonatomic,strong) NSString *comboNameProperty;

@property(nonatomic,strong) NSArray *comboArray;

@property (weak, nonatomic) IBOutlet UILabel *titleBar;

- (IBAction)btnClosePressed:(id)sender;

@end


@protocol ComboDelegate<NSObject>
@optional
- (void) userFinishedChoosingitem :(NSDictionary*) item andType:(NSString *)type;
- (void)closeButtonClicked;
@end
