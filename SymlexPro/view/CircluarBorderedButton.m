//
//  CircluarBorderedButton.m
//  SymlexPro
//
//  Created by USER on 8/13/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "CircluarBorderedButton.h"
#import "ColorUtility.h"

@implementation CircluarBorderedButton

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self addAction];
    }
    return self;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self addAction];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addAction];
    }
    return self;
}

- (void)addAction {
    [self addTarget:self action:@selector(pressed:) forControlEvents:UIControlEventTouchDown];
    [self addTarget:self action:@selector(pressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addTarget:self action:@selector(pressed:) forControlEvents:UIControlEventTouchUpOutside];
    [self addTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventTouchCancel];
    [self addTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventPrimaryActionTriggered];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.adjustsImageWhenHighlighted = NO;
    
    self.layer.borderWidth = 2;
    
    if(self.borderColor){
        
        self.layer.borderColor = self.borderColor.CGColor;
    }else{
        self.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    
    self.layer.cornerRadius = self.layer.frame.size.width/2;
    
}

-(void) addBackgroundColor :(NSString *)firstColorCode{
    
    self.backgroundColor = [ColorUtility colorWithHexString:firstColorCode];
    
}

- (void)pressed:(UIButton *)btn {
    
    [btn setBackgroundColor:[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1]];
    
}

- (void)touchUp:(UIButton *)btn {
    
    [btn setBackgroundColor:[UIColor whiteColor]];
    
}

@end
