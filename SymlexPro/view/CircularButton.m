//
//  CircularButton.m
//  SymlexPro
//
//  Created by USER on 8/14/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "CircularButton.h"

@implementation CircularButton

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self addAction];
    }
    return self;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self addAction];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addAction];
    }
    return self;
}

- (void)addAction {
    [self addTarget:self action:@selector(pressed:) forControlEvents:UIControlEventTouchDown];
    [self addTarget:self action:@selector(pressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addTarget:self action:@selector(pressed:) forControlEvents:UIControlEventTouchUpOutside];
    [self addTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventTouchCancel];
    [self addTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventPrimaryActionTriggered];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.adjustsImageWhenHighlighted = NO;
    self.layer.cornerRadius = self.layer.frame.size.width/2;
    
}

- (void)pressed:(UIButton *)btn {
   // [btn setBackgroundColor:[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1]];
}

- (void)touchUp:(UIButton *)btn {
   // [btn setBackgroundColor:[UIColor whiteColor]];
}

@end
