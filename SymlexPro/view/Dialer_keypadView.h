//
//  Dialer_keypadView.h
//  ipjsua
//
//  Created by Riaz Hasan on 9/19/15.
//  Copyright (c) 2015 Teluu. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol KeyPadDelegate;

@interface Dialer_keypadView : UIViewController
{
    // Delegate to respond back
}

@property (nonatomic, assign) id<KeyPadDelegate> delegate;

- (IBAction)myClickEvent:(UIButton *)sender;

@end

// 3. Definition of the delegate's interface
@protocol KeyPadDelegate <NSObject>

- (void)keypadPressedWithValue:(NSString*)value;

@end
