//
//  MorApi.h
//  Symlex
//
//  Created by Sijan's Mac on 22/1/18.
//  Copyright © 2018 Kolpolok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PostDataController.h"

@interface MorApi : NSObject <PostDataControllerDelegate>

-(void) registerUser:(id)sender;

@end
