
#import "SettingsViewController.h"
#import "DialpadViewController.h"
#import "Constans.h"
#import "MyApp.h"
#import "WelcomeViewController.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"

@interface SettingsViewController (){
    
    NSUserDefaults *userDefault;
}

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefault = [NSUserDefaults standardUserDefaults];
    // Do any additional setup after loading the view.
    
    //register an observer for registration callback coming from myapp.m
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processRegState:)
                                                 name: kSIPRegState
                                               object:nil];
    
    [self initStatusButton:@"red_status"];
    
}

-(void)viewWillAppear:(BOOL)animated{
    if([[userDefault objectForKey:@"isIpv6"] isEqualToString:@"true"]){
        self.coverView.hidden = false;
    }else{
        self.coverView.hidden = true;
    }
}

-(void) viewDidAppear:(BOOL)animated{
    
    get_account_status();
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) initStatusButton :(NSString *) imageName{
    
    UIImage *image = [UIImage imageNamed:imageName];
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,15, 15)];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    button.adjustsImageWhenHighlighted = NO;
    UIBarButtonItem *leftButton =[[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = leftButton;
    
}


- (void)processRegState:(NSNotification *)notification
{
    NSDictionary *dictionary = [notification userInfo];
    
    //  NSLog(@"Status in phoneview: %d",[[dictionary objectForKey:@"Status"] intValue]);
    if ([[dictionary objectForKey:@"Status"] intValue] == 200){
        [self initStatusButton:@"green_status"];
    }
    else if ([[dictionary objectForKey:@"StatusText"] isEqual:@"registersent"]){
        [self initStatusButton:@"yellow_status"];
    }
    else if ([[dictionary objectForKey:@"Status"] intValue] == 403){
        [self initStatusButton:@"red_status"];
    }
    else if ([[dictionary objectForKey:@"Status"] intValue] == 404){
        [self initStatusButton:@"red_status"];
    }else if ([[dictionary objectForKey:@"Status"] intValue] == 407){
        [self initStatusButton:@"red_status"];
    }
    
    else{
        [self initStatusButton:@"red_status"];
    }
    
}


- (IBAction)tellAFriendPressed:(id)sender {
    
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        controller.recipients = [NSArray arrayWithObjects:nil, nil];
        controller.messageComposeDelegate = self;
        controller.body = @"Lets make free and cheap international call from Zuniro. Download Zuniro from https://itunes.apple.com/us/app/zuniro/id1289551870?ls=1&mt=8";
        [self presentViewController:controller animated:YES completion:nil];
    }
    
}

- (IBAction)dialpadPressed:(id)sender {
    DialpadViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"dialpad"];
    [self presentViewController:viewController animated:YES completion:nil];
}

-(void) messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    if (result == MessageComposeResultCancelled){
        NSLog(@"Message cancelled");
    }
    else if (result == MessageComposeResultSent){
        NSLog(@"Message sent");
    }
    else{
        NSLog(@"Message failed");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)rateDisplayPressed:(id)sender {
    
    [self showRateAlert];
}



-(void)showRateAlert{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"To see rate enter prefix"
                                                                              message: nil
                                                        preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter prefix here";
        //textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        //textField.borderStyle = UITextBorderStyleRoundedRect;
        [textField setKeyboardType:UIKeyboardTypeDecimalPad];
    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * namefield = textfields[0];
        NSLog(@"%@",namefield.text);
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Search" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        NSString *userName = [userDefault stringForKey:data_user_name];
        NSString *url = @"http://sip.shellvoice.com/billing/api/rate";
        PostDataController *controller = [[PostDataController alloc] init];
        controller.delegate = self;
        NSDictionary *param = @{@"u": @"appconnect",
                                @"username": userName,
                                @"prefix":alertController.textFields[0].text
                                };
        [controller postwithURL:url withData:param withTag:101];
        
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


-(void)postFinishedWithData:(id)responseObject withTag:(int)tag{
    
    if(tag == 101){
        XMLDictionaryParser *parser = [[XMLDictionaryParser alloc] init];
        NSDictionary *dic = [parser dictionaryWithParser:responseObject];
        
        NSString *error = [dic valueForKeyPath:@"error"];
        
        if(error != nil){
            NSLog(@"MyDic Error--%@",[dic valueForKeyPath:@"error"]);
            [self showAlert:nil withMsg:[dic valueForKeyPath:@"error"] withTag:0];
            return;
        }
        
        NSString *rate = [dic valueForKeyPath:@"rate"];
        if(rate != nil){
            NSLog(@"MyDic Rates--%@",[dic valueForKeyPath:@"rate"]);
            NSArray *array = [rate componentsSeparatedByString:@"#"];
            NSString *rate = [NSString stringWithFormat:@"Prefix: %@\nCountry: %@\nRate: %@$/min", array[2],array[1],array[0]];
            [self showAlert:nil withMsg:rate withTag:1];
        }
    }
}

-(void)postFinishedWithError:(NSError *)error{
    NSLog(@"error---%@",error);
}


-(void)showAlert: (NSString*)title withMsg:(NSString*)msg withTag:(int)tag {
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: title
                                                                              message: msg
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    
    if(tag == 3){
    
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
            //Cancel pressed
        }]];
    }
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if(tag == 1){
            [self showRateAlert];
        }
        else if(tag == 3){
            [SVProgressHUD showWithStatus:@"please wait.."];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                pjsua_destroy();
                [userDefault setObject:FALSE forKey:data_isAccountCreated];
                AppDelegate *app = (AppDelegate *) [[UIApplication sharedApplication] delegate];
                UIViewController *uc = [self.storyboard instantiateViewControllerWithIdentifier:@"welcomeView"];
                UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:uc];
                app.window.rootViewController = navController;
                [SVProgressHUD dismiss];
            });
        }
        
        
    }]];
    
    
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)logoutPressed:(id)sender {
    [self showAlert:@"Logout" withMsg:@"Do you really want to logout?" withTag:3];
}


@end
