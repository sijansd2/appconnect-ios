//
//  NetworkManager.m
//  SymlexPro
//
//  Created by admin on 9/17/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "NetworkManager.h"
#import "Reachability.h"
#import "AFNetworking.h"
#import "Constans.h"
#import "NSNotificationAdditions.h"
@implementation NetworkManager


Boolean network;
static NetworkManager *instance = nil;

+(NetworkManager*)getInstance{
    if(!instance){
        instance = [[NetworkManager alloc]init];
        [instance startMonitoring];
    }
    
    return instance;
}


+(Boolean)getNetworkStatus{
    
    if(!instance){
        instance = [[NetworkManager alloc]init];
        [instance startMonitoring];
    }
    
    return network;
}


-(void)startMonitoring{
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [self setNetworkStatus];
    
}

-(void) setNetworkStatus{

    
    AFNetworkReachabilityManager *reachability = [AFNetworkReachabilityManager sharedManager];
    [reachability setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:
                NSLog(@"Internet is WWN");
                network = true;
                
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                NSLog(@"Internet is WiFi");
                network = true;
                break;
            case AFNetworkReachabilityStatusUnknown:
                NSLog(@"Internet is Unknown");
                network = true;
                
                break;
            case AFNetworkReachabilityStatusNotReachable:
                NSLog(@"Internet is Not Reachable");
                
                network = false;
                break;
            default:
                
                network = false;
                break;
        }
        
        NSDictionary *userinfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSNumber numberWithBool:network], @"netStat",
                                  nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:NetworkUpdate
                                                                            object:nil
                                                                          userInfo:userinfo];
    }];
    
   

    
    
}




@end
