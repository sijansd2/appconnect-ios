//
//  SettingsViewController.h
//  SymlexPro
//
//  Created by admin on 9/18/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "PostDataController.h"

@interface SettingsViewController : UIViewController<MFMessageComposeViewControllerDelegate, PostDataControllerDelegate>

- (IBAction)tellAFriendPressed:(id)sender;
- (IBAction)dialpadPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnRate;
- (IBAction)rateDisplayPressed:(id)sender;
- (IBAction)logoutPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *coverView;

@end
