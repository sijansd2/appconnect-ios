//
//  MorCountry.h
//  ShellVoice
//
//  Created by Sijan's Mac on 29/1/18.
//  Copyright © 2018 Kolpolok. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MorCountry : NSObject

@property (nonatomic,strong) NSArray * countryName;
@property (nonatomic,strong) NSArray * countryCode;
@property (nonatomic,strong) NSArray * countryId;

@end
