//
//  ForgetPassViewController.m
//  ShellVoice
//
//  Created by Sijan's Mac on 7/2/18.
//  Copyright © 2018 Kolpolok. All rights reserved.
//

#import "ForgetPassViewController.h"
#import "SVProgressHUD.h"

@interface ForgetPassViewController ()

@end

@implementation ForgetPassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.webView.delegate = self;
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://sip.shellvoice.com/billing/images/callc/login"]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)webViewDidStartLoad:(UIWebView *)webView{
    
    [SVProgressHUD showWithStatus:@"please wait.."];
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [SVProgressHUD dismiss];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    NSLog(@"Web view Error  %@",error);
}

@end
