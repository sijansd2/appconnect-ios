//
//  CallLogViewController.h
//  SymlexPro
//
//  Created by admin on 7/31/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CallLogViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *callLogTable;

-(IBAction) btnDialpadPressed:(id)sender;

@end
