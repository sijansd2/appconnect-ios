//
//  ContentViewController.m
//  ShasthoNet
//
//  Created by Riaz on 6/22/16.
//  Copyright © 2016 Riaz. All rights reserved.
//

#import "ContentViewController.h"

@interface ContentViewController (){

    NSArray *titleArray;
    NSArray *subtitleArray;
    NSArray *imgArray;
    
}

@end

@implementation ContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    titleArray = [NSArray arrayWithObjects:@"Wellcome to Zuniro",@"Zuniro features",@"More features", nil];
    
    subtitleArray = [NSArray arrayWithObjects:@"Free Calls, International Calls, Text Message",@"Easy to Signup",@"Vouchar Recharge, Balance Share, Low bandwidth and Secured communication", nil];
    
    imgArray = [NSArray arrayWithObjects:@"slid1.png",@"slid2.png",@"slid3.png", nil];

    
    self.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",imgArray[self.pageIndex]]];
    self.lbTitle.text = titleArray[self.pageIndex];
    self.lbSubtitle.text = subtitleArray[self.pageIndex];
    
//    if(self.pageIndex ==0){
//    
//        self.ima
//    }
//    if(self.pageIndex ==1){
//        
//        self.view.backgroundColor = [UIColor grayColor];
//    }
//    if(self.pageIndex ==2){
//        
//        self.view.backgroundColor = [UIColor blueColor];
//    }
//    if(self.pageIndex ==3){
//        
//        self.view.backgroundColor = [UIColor orangeColor];
//    }
//    if(self.pageIndex ==4){
//        
//        self.view.backgroundColor = [UIColor whiteColor];
//    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)prefersStatusBarHidden {
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
