//
//  PaypalViewController.h
//  ShellVoice
//
//  Created by Sijan's Mac on 30/1/18.
//  Copyright © 2018 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PayPalMobile.h"
#import "PostDataController.h"

@interface PaypalViewController : UIViewController <PayPalPaymentDelegate,PostDataControllerDelegate>

@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, strong, readwrite) NSString *resultText;
- (IBAction)rechargePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *tfAmount;

@end
