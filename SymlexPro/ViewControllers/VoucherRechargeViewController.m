
#import "VoucherRechargeViewController.h"
#import "Constans.h"
#import "MyEncryption.h"
#import "NetworkManager.h"
#import "UIView+Toast.h"
#import "XMLDictionary.h"
#import "NSNotificationAdditions.h"

@interface VoucherRechargeViewController (){
    
    NSUserDefaults *userDefault;
}

@end

@implementation VoucherRechargeViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    userDefault = [NSUserDefaults standardUserDefaults];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    //Tap gesture enable
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiskeyboard)];
    [self.view addGestureRecognizer:tap];

    [self updateView];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//MARK: Alert
-(void) showVoucherAlert:(NSString*)title messageBody:(NSString*)msg{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Ok"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    //NSLog(@"you pressed Yes button");
                                    NSDictionary *userinfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                                              @"aaa",@"ssss",
                                                              nil];
                                    
                                    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kBalanceState
                                                                                                        object:nil
                                                                                                      userInfo:userinfo];
                                    
                                }];
    
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}


//MARK: Preessed Functions
- (IBAction)cancelPressed:(id)sender {
    [self dismiskeyboard];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)rechargePressed:(id)sender {

    Boolean network = [NetworkManager getNetworkStatus];
    if(network){
        [self dismiskeyboard];
        [self voucharRecharge];
        
    }
    else{
        [self.view makeToast:@"Please Check Your Internet"];
    }
    
}


//MARK: Functions
-(void)voucharRecharge{
    
    NSString *userId = [userDefault stringForKey:data_morUserId];
    NSString *url = @"http://sip.shellvoice.com/billing/api/voucher_use";
    PostDataController *controller = [[PostDataController alloc] init];
    controller.delegate = self;
    NSDictionary *param = @{@"u": @"admin",
                            @"user_id": userId,
                            @"voucher_number": self.tfPinNumber.text
                            };
    [controller postwithURL:url withData:param withTag:101];
}

//MARK: Http post callbacks
-(void)postFinishedWithData:(id)responseObject withTag:(NSUInteger)tag{
    
    if(tag == 101){
        XMLDictionaryParser *parser = [[XMLDictionaryParser alloc] init];
        NSDictionary *dic = [parser dictionaryWithParser:responseObject];
        //NSLog(@"Voucher Recharge--%@",dic);
        NSString *error = [dic valueForKeyPath:@"status.error"];
        
        if(error != nil){
            NSLog(@"MyDic Error--%@",error);
            [self showVoucherAlert:@"Server Response" messageBody:error];
            return;
        }
        
        NSString *status = [dic valueForKeyPath:@"status.status"];
//        NSString *voucher_number = [dic valueForKeyPath:@"status.voucher_number"];
//        NSString *voucher_id = [dic valueForKeyPath:@"status.voucher_id"];
//        NSString *credit_with_tax = [dic valueForKeyPath:@"status.credit_with_tax"];
//        NSString *credit_without_tax = [dic valueForKeyPath:@"status.credit_without_tax"];
        NSString *currency = [dic valueForKeyPath:@"status.currency"];
//        NSString *credit_in_default_currency = [dic valueForKeyPath:@"status.credit_in_default_currency"];
//        NSString *user_id = [dic valueForKeyPath:@"status.user_id"];
        NSString *balance_after_voucher_use = [dic valueForKeyPath:@"status.balance_after_voucher_use"];
//        NSString *payment_id = [dic valueForKeyPath:@"status.payment_id"];
        
        if(status != nil){
            NSString *msg = [NSString stringWithFormat:@"Recharge successful with balance %@ %@",balance_after_voucher_use,currency];
            [self showVoucherAlert:@"Success" messageBody:msg];
        }
    }
}

-(void)postFinishedWithError:(NSError *)error{
    NSLog(@"%@",error);
    //[self showVoucherAlert:@"Error" messageBody:[error.userInfo valueForKey:@"NSLocalizedDescription"]];
}



-(void)dismiskeyboard{
    [self.tfPinNumber resignFirstResponder];
}

-(void)updateView{
    self.btnRecharge.enabled = false;
    [self.tfPinNumber addTarget:self action:@selector(updateRechargeButton) forControlEvents:UIControlEventEditingChanged];
}

-(void)updateRechargeButton{
    BOOL textFieldsIsValid = self.tfPinNumber.text.length > 0;
    self.btnRecharge.enabled = textFieldsIsValid;
}

@end
