//
//  WelcomeViewController.h
//  Symlex
//
//  Created by Sijan's Mac on 24/1/18.
//  Copyright © 2018 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeViewController : UIViewController

- (IBAction)signupPressed:(id)sender;
- (IBAction)signInPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *signUpBtn;

@end
