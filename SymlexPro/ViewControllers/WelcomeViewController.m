//
//  WelcomeViewController.m
//  Symlex
//
//  Created by Sijan's Mac on 24/1/18.
//  Copyright © 2018 Kolpolok. All rights reserved.
//

#import "WelcomeViewController.h"
#import "TabViewController.h"
#import "Constans.h"
#import "AppDelegate.h"
#import "ColorUtility.h"
#import "AFNetworking.h"

@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    UIColor *bgcolor =[ColorUtility colorWithHexString:navBarColor];
    self.navigationController.navigationBar.barTintColor = bgcolor;
    
    NSUserDefaults *userDefault =[NSUserDefaults standardUserDefaults];
    //[self getDataFromApi];
    
    if([userDefault objectForKey:data_isAccountCreated] != nil &&
       [[userDefault objectForKey:data_isAccountCreated] isEqualToString:@"true"]){
        
        TabViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tabview"];
        AppDelegate *app = (AppDelegate *) [[UIApplication sharedApplication] delegate];
        viewController.isFirstLaunch = TRUE;
        [app.window setRootViewController:viewController];
        
    }
    
    // To check ipv6
    [self getAppReleaseConfig];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// To Check ipv6 config
-(void) getAppReleaseConfig{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSString *url = @"http://conf.aussdjf.xyz/iosapi/appconnect/ios_api.html";
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"%@",responseObject);
        NSString *aStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        if([aStr containsString:@"\n"]){
            aStr = [aStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        }
        
        if([aStr containsString:@"\r"]){
            aStr = [aStr stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        }
        
        NSString * appBuildNum = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
        NSLog(@"appBuildNum-------------%@----%@",appBuildNum,aStr);
        if (aStr != nil && [aStr isEqualToString:appBuildNum])
        {
            NSUserDefaults *userDef= [NSUserDefaults standardUserDefaults];
            [userDef setObject:@"true" forKey:@"isIpv6"];
            [self.signUpBtn setHidden:YES];
        }else{
            
            NSUserDefaults *userDef= [NSUserDefaults standardUserDefaults];
            [userDef setObject:@"false" forKey:@"isIpv6"];
            [self.signUpBtn setHidden:NO];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
        [self.signUpBtn setHidden:NO];
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)signupPressed:(id)sender {
    //[self performSegueWithIdentifier:@"signupId" sender:sender];
}

- (IBAction)signInPressed:(id)sender {
    //[self performSegueWithIdentifier:@"signinId" sender:sender];
}
@end
