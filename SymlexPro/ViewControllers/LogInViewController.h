//
//  SignUpViewController.h
//  SymlexPro
//
//  Created by admin on 7/18/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ComboView.h"
#import "WhiteBorderedButton.h"
#import "PostDataController.h"
#import "GetDataController.h"
#import "XMLParser.h"

@interface LogInViewController : UIViewController<UITextFieldDelegate,ComboDelegate,PostDataControllerDelegate,GetDataControllerDelegate,XMLParserDelegate,NSXMLParserDelegate,UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *pickerContainer;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (weak, nonatomic) IBOutlet WhiteBorderedButton *btnSignUp;
@property (weak, nonatomic) IBOutlet UITextField *tfOppcode;
@property (weak, nonatomic) IBOutlet UITextField *tfUsername;

- (IBAction)pickerDonePressed:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
//@property (weak, nonatomic) IBOutlet UITextField *tfConfirmPassword;
- (IBAction)btnSignUpPressed:(id)sender;
@end
