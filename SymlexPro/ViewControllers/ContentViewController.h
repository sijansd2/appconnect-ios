//
//  ContentViewController.h
//  ShasthoNet
//
//  Created by Riaz on 6/22/16.
//  Copyright © 2016 Riaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentViewController : UIViewController

@property NSUInteger pageIndex;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbSubtitle;

@end
