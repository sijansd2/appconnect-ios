//
//  SideViewController.m
//  SymlexPro
//
//  Created by admin on 7/26/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "SideViewController.h"
#import "REFrostedViewController.h"
#import "AppDelegate.h"
@interface SideViewController (){

    NSMutableArray *menuNameArray;
    NSMutableArray *menuImageArray;
    
}

typedef NS_ENUM(NSInteger, SideMenuTag) {
    SideMenuVoucherRecharge = 0,
    SideMenuBalanceTransfer,
    SideMenuChangePass,
    SideMenuSecretPin,
    SideMenuPrivacy,
    SideMenuSignOut,
    SideMenuExit
};

@end

@implementation SideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUPMenu];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUPMenu{
    
    menuNameArray = [NSMutableArray arrayWithObjects:@"Voucher Recharge",@"Balance Transfer",@"Change Password",@"Secret Pin Reset",@"Privacy Policy",@"Sign Out",@"Exit", nil];
    
  //  menuImageArray = [NSMutableArray arrayWithObjects:@"Settings",@"RateApp",@"Feedback",@"ShareGrade",@"GoPremium",@"PrivacyPolicy",@"Logout", nil];
    
    [self.tableView reloadData];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [menuNameArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier=@"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    UILabel *menuName = [cell viewWithTag:100];
    UIImageView *imageView = [cell viewWithTag:101];
    
    menuName.text = menuNameArray[indexPath.row];
    imageView.image = [UIImage imageNamed:menuImageArray[indexPath.row]];
    
    
    return cell;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.row == SideMenuVoucherRecharge){
        [self openDrawerItems:@"voucherRecharge"];
    }else if(indexPath.row == SideMenuPrivacy){
       
    }else if(indexPath.row == SideMenuBalanceTransfer){
        [self openDrawerItems:@"balanceTransfer"];
    }else if(indexPath.row == SideMenuChangePass){
        [self openDrawerItems:@"changePass"];
    }else if(indexPath.row == SideMenuSecretPin){
        
    }else if(indexPath.row == SideMenuPrivacy){
       
    }else if(indexPath.row == SideMenuSignOut){
        
    }else if(indexPath.row == SideMenuExit){
        
    }
    
    [self.frostedViewController hideMenuViewController];
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 50;
}

-(void)openDrawerItems: (NSString*) itemName{
    UINavigationController * navController = [self.storyboard instantiateViewControllerWithIdentifier:itemName];
    [self presentViewController:navController animated:YES completion:nil];
}



@end
