//
//  PaypalViewController.m
//  ShellVoice
//
//  Created by Sijan's Mac on 30/1/18.
//  Copyright © 2018 Kolpolok. All rights reserved.
//

#import "PaypalViewController.h"
#import "XMLDictionary.h"
#import "Constans.h"
#import "NSNotificationAdditions.h"


#define kPayPalEnvironment PayPalEnvironmentNoNetwork

@interface PaypalViewController ()

@property(nonatomic, strong, readwrite) IBOutlet UIView *successView;
@property(nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;

@end

@implementation PaypalViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    //Tap gesture enable
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiskeyboard)];
    [self.view addGestureRecognizer:tap];
    
    [self configPaypalPayment];
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.environment = PayPalEnvironmentProduction;
    [PayPalMobile preconnectWithEnvironment:self.environment];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) configPaypalPayment{
    
    _payPalConfig = [[PayPalConfiguration alloc] init];
    _payPalConfig.acceptCreditCards = NO;
    _payPalConfig.merchantName = @"App Connect";
    _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
    _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
    _payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    _payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
    
    self.environment = kPayPalEnvironment;
    NSLog(@"PayPal iOS SDK version: %@", [PayPalMobile libraryVersion]);
}

- (IBAction)rechargePressed:(id)sender {
    
    NSDecimalNumber *total = [NSDecimalNumber decimalNumberWithString:self.tfAmount.text];
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = total;
    payment.currencyCode = @"USD";
    payment.items = nil;
    payment.shortDescription = @"Shell Voice payment";
    
    if (!payment.processable) {
        NSLog(@"Payment not possible");
        return;
    }
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                configuration:self.payPalConfig
                                                                                                     delegate:self];
    [self presentViewController:paymentViewController animated:YES completion:nil];
}


//MARK: PayPal Payment Delegate
-(void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment{
    
    NSLog(@"PayPal Payment Success!");
    self.resultText = [completedPayment description];
    
    [self sendCompletedPaymentToServer:completedPayment];
    // Payment was processed successfully; send to server for verification and fulfillment
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    self.resultText = nil;
    self.successView.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment {
    
    NSDictionary *response = [completedPayment.confirmation objectForKey:@"response"];
    NSString *paymentId = [response objectForKey:@"id"];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *userName = [userDefault stringForKey:data_user_name];
    NSString *userEmail = [userDefault stringForKey:data_regEmail];
    NSString *userId = [userDefault stringForKey:data_morUserId];
    NSString *url = @"http://sip.shellvoice.com/billing/api/payment_create";
    PostDataController *controller = [[PostDataController alloc] init];
    controller.delegate = self;
    
    if(userName != nil && userId != nil && userEmail != nil && paymentId != nil){
    
        NSDictionary *param = @{@"u": @"admin",
                            @"user_id": userId,
                            @"p_currency": @"USD",
                            @"paymenttype": @"paypal",
                            @"tax_in_amount": @"1",
                            @"amount": self.tfAmount.text,
                            @"transaction":paymentId,
                            @"payer_email":userEmail,
                            @"u": @"admin"
                            };
    
        NSLog(@"Param:--%@",param);
        [controller postwithURL:url withData:param withTag:101];
    
    }

}



//MARK: Http post delegate
-(void)postFinishedWithData:(id)responseObject withTag:(NSUInteger)tag{
    
    if(tag == 101){
        XMLDictionaryParser *parser = [[XMLDictionaryParser alloc] init];
        NSDictionary *dic = [parser dictionaryWithParser:responseObject];
        //NSLog(@"Voucher Recharge--%@",dic);
        NSString *error = [dic valueForKeyPath:@"response.error"];
        
        if(error != nil){
            NSLog(@"MyDic Error--%@",error);
            [self showAlert:@"Error" messageBody:error withTag:0];
            return;
        }
        
        NSString *status = [dic valueForKeyPath:@"response.status"];
        if([status isEqualToString:@"ok"]){
            NSLog(@"Payment Response:-- %@",dic);
            [self showAlert:@"Success" messageBody:@"Payment successful" withTag:3];
        }
        
    }
}

-(void)postFinishedWithError:(NSError *)error{
    NSLog(@"%@",error);
    [self showAlert:@"Error" messageBody:[NSString stringWithFormat:@"%@",error] withTag:0];
}

//MARK: Alert
-(void) showAlert:(NSString*)title messageBody:(NSString*)msg withTag:(int)tag{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Ok"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    if(tag == 3){
                                        NSDictionary *userinfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                  @"aaa",@"ssss",
                                                                  nil];
                                        
                                        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kBalanceState
                                                                                                            object:nil
                                                                                                          userInfo:userinfo];
                                        [self.navigationController popViewControllerAnimated:YES];
                                    }
                                    
                                }];
    
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)dismiskeyboard{
    [self.tfAmount resignFirstResponder];
}

@end
