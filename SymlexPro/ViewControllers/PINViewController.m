//
//  PINViewController.m
//  SymlexPro
//
//  Created by USER on 9/14/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import "PINViewController.h"
#import "Constans.h"
#import "MyEncryption.h"
#import "TabViewController.h"
#import "UIView+Toast.h"
#import "NetworkManager.h"

@interface PINViewController ()

@end

@implementation PINViewController{
    
    NSUserDefaults *userDefault;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    userDefault = [NSUserDefaults standardUserDefaults];
    [self updateView];
    
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    [self updateSaveButton];
}

-(void) updateView{
    
    if(self.isFirstTime){
        self.tfCurrentPin.hidden = YES;
        self.currentPinHeightConstraint.constant =0;
        self.lbFrontMessage.text = @"Set your secret pin. \nThis pin will used to Balance share , \nvoucher recharge etc";
        self.lbFrontMessage.font=[UIFont fontWithName:@"Helvetica" size:16];
    }
    
    [self.tfCurrentPin addTarget:self action:@selector(updateSaveButton) forControlEvents:UIControlEventEditingChanged];
    [self.tfNewPin addTarget:self action:@selector(updateSaveButton) forControlEvents:UIControlEventEditingChanged];
    [self.tfConfirmPin addTarget:self action:@selector(updateSaveButton) forControlEvents:UIControlEventEditingChanged];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)btnSavePressed:(id)sender{
    
    Boolean network = [NetworkManager getNetworkStatus];
    
    if(network){
        
        PostDataController *controller = [[PostDataController alloc] init];
        controller.delegate = self;
        
        if(self.isFirstTime){
            
            NSString *url= [NSString stringWithFormat: @"%@%@",baseUrl,setPin];
            NSDictionary *param = @{@"username": [userDefault objectForKey:data_user_name],
                                    @"pin_code": self.tfNewPin.text,
                                    @"api_key":apiKey
                                    };
            [controller postwithURL:url withData:param withTag:100];
        }else{
            
            NSString *url= [NSString stringWithFormat: @"%@%@",baseUrl,resetPin];
            NSString* md5Str = [[NSString stringWithFormat:@"%@%@",[userDefault objectForKey:data_user_name],[userDefault objectForKey:data_password]] MD5String];
            
            NSDictionary *param = @{@"username": [userDefault objectForKey:data_user_name],
                                    @"password": [userDefault objectForKey:data_password],
                                    @"sec_key": md5Str,
                                    @"old_pin": self.tfCurrentPin.text,
                                    @"new_pin": self.tfNewPin.text
                                    };
            [controller postwithURL:url withData:param withTag:101];
            
        }
    }else{
        [self.view makeToast:@"Please Check Your Internet"];
    }
    
    
    
}

-(void) postFinishedWithData:(id)responseObject withTag:(NSUInteger)tag{
    
    if(responseObject != nil){
        if(tag == 100){
            //new pin
            if([responseObject[@"message"] isEqualToString:@"Success"]){
                
                [self.view makeToast:@"Pin successfully saved"];
                [userDefault setObject:@"true" forKey:data_isAccountCreated];
                TabViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tabview"];
                [self.navigationController pushViewController:viewController animated:YES];
                
                self.tfCurrentPin.text = @"";
                self.tfNewPin.text = @"";
                self.tfConfirmPin.text = @"";
                
            }else{
                [self.view makeToast:responseObject[@"message"]];
                
            }
            
        }else if(tag == 101){
            //change pin
            if([responseObject[@"message"] isEqualToString:@"success"]){
                [self.view makeToast:responseObject[@"message"]];
            }
            else{
                [self.view makeToast:responseObject[@"message"]];
            }
            
        }
        
    }
}

-(void) postFinishedWithError:(NSError *)error{
    
    [self.view makeToast:[error.userInfo valueForKey:@"NSLocalizedDescription"]];
    
}

- (void)updateSaveButton {
    BOOL changePin = self.tfCurrentPin.text.length > 0 && self.tfNewPin.text.length > 0 && [self.tfNewPin.text isEqualToString:self.tfConfirmPin.text];
    
    BOOL firsttime =  self.tfNewPin.text.length > 0 && [self.tfNewPin.text isEqualToString:self.tfConfirmPin.text];
    
    self.btnSave.enabled = self.isFirstTime? firsttime:changePin;
    
    if(self.tfCurrentPin.text.length > 0 && self.tfNewPin.text.length > 0 && self.tfConfirmPin.text.length>0){
    
        if(![self.tfNewPin.text isEqualToString:self.tfConfirmPin.text]){
        
            self.lbStatus.text = @"New pin and Confirm Pin mismatch";
        }else{
        
            self.lbStatus.text = @"";
        }
    
    }else{
    
        self.lbStatus.text = @"";
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)dismissKeyboard {
    //[tvOpcode resignFirstResponder];
    [self.tfCurrentPin resignFirstResponder];
    [self.tfNewPin resignFirstResponder];
    [self.tfConfirmPin resignFirstResponder];
}

@end
