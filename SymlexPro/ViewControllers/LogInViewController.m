
#import "LogInViewController.h"
#import "ViewExtension.h"
#import "TextFieldUtils.h"
#import "CountryDataSource.h"
#import "UIViewController+MJPopupViewController.h"
#import "LoginManager.h"
#import "Constans.h"
#import "MyEncryption.h"
#import "OtpViewController.h"
#import "UIView+Toast.h"
#import "MainPageViewController.h"
#import "SVProgressHUD.h"
#import "Base64.h"
#import "TabViewController.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "NetworkManager.h"
#import "MorApi.h"
#import "XMLDictionary.h"
#import "WelcomeViewController.h"
#import "MorCountry.h"

#define kOFFSET_FOR_KEYBOARD 80.0

@interface LogInViewController (){

    NSUserDefaults *userDefault;
    NSArray * countryName;
    NSArray * countryCode;
    NSArray * countryId;
    int position;
    UITapGestureRecognizer* tap;
}

@property (strong, nonatomic) NSArray *countryObjects;

@end

@implementation LogInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    userDefault =[NSUserDefaults standardUserDefaults];
    position = 0;
    [self updateView];
    
    //Tap gesture enable
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiskeyboard)];
    [self.view addGestureRecognizer:tap];
    MorCountry *morCountry = [[MorCountry alloc] init];
    countryName = morCountry.countryName;
    countryCode = morCountry.countryCode;
    countryId = morCountry.countryId;
    
    self.picker.dataSource = self;
    self.picker.delegate = self;
    
    
    NSLog(@"isipv6 %@",[userDefault objectForKey:@"isIpv6"]);
    if([[userDefault objectForKey:@"isIpv6"] isEqualToString:@"true"]){
        self.tfOppcode.hidden = YES;
    }else{
        self.tfOppcode.hidden = NO;
    }
    

}

-(void) viewWillAppear:(BOOL)animated{

    if([userDefault objectForKey:data_isAccountCreated] != nil &&
       [[userDefault objectForKey:data_isAccountCreated] isEqualToString:@"true"]){
        
        TabViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tabview"];
        AppDelegate *app = (AppDelegate *) [[UIApplication sharedApplication] delegate];
        [app.window setRootViewController:viewController];
        [viewController setSelectedIndex:1];
        
    }
    
}

-(void)showPickerView{
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.pickerContainer.bounds];
    self.pickerContainer.layer.masksToBounds = NO;
    self.pickerContainer.layer.shadowColor = [UIColor blackColor].CGColor;
    self.pickerContainer.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    self.pickerContainer.layer.shadowOpacity = 0.5f;
    self.pickerContainer.layer.shadowPath = shadowPath.CGPath;
    self.pickerContainer.center = self.view.center;
    [self.view addSubview:self.pickerContainer];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return countryName.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return countryName[row];
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = countryName[row];
    NSAttributedString *attString =
    [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"%ld", (long)row);
    position = (int)row;
}



-(BOOL) textFieldShouldBeginEditing:(UITextField *)textField{
    
    if(textField == self.tfOppcode){
        [self showPickerView];
        return NO;
    }
    return YES;
}

-(void) updateView{
    
    [TextFieldUtils addImageLeftSideTextView:self.tfOppcode imageName:@"flag.png"];
    [TextFieldUtils addImageRightSideTextView:self.tfOppcode imageName:@"arrow_right.png"];
    [TextFieldUtils addImageLeftSideTextView:self.tfUsername imageName:@"phone.png"];
    [TextFieldUtils addImageLeftSideTextView:self.tfPassword imageName:@"password.png"];
    
    self.tfOppcode.delegate=self;
    self.tfUsername.delegate=self;
    self.tfPassword.delegate=self;
    
    self.tfOppcode.textColor = [UIColor whiteColor];
    self.tfPassword.textColor = [UIColor whiteColor];
    self.tfUsername.textColor = [UIColor whiteColor];
    
    [self.tfOppcode addTarget:self action:@selector(updateLogInButton) forControlEvents:UIControlEventEditingChanged];
    [self.tfUsername addTarget:self action:@selector(updateLogInButton) forControlEvents:UIControlEventEditingChanged];
    [self.tfPassword addTarget:self action:@selector(updateLogInButton) forControlEvents:UIControlEventEditingChanged];
    
    [self updateLogInButton];
}


-(void)setCountryCode:(int) position{
    
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    CGSize stringBoundingBox = [countryCode[position] sizeWithFont:font];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 35+stringBoundingBox.width, 40)];
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(10, 8, 24, 24)];
    imageview.image = [UIImage imageNamed:@"phone.png"];
    imageview.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:imageview];
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(35, 0, stringBoundingBox.width, 39)];
    label.text = countryCode[position];
    label.textColor=[UIColor whiteColor];
    label.font = font;
    [view addSubview:label];
    self.tfUsername.leftViewMode = UITextFieldViewModeAlways;
    self.tfUsername.leftView = view;
    [self.tfUsername clipsToBounds];
    
    [self.pickerContainer removeFromSuperview];
    self.tfOppcode.text = countryName[position];
}

- (IBAction)btnSignUpPressed:(id)sender {
    [self.view endEditing:YES];
    
    Boolean network = [NetworkManager getNetworkStatus];
    if(network){
        if([[userDefault objectForKey:@"isIpv6"] isEqualToString:@"true"]){
            [self getXmlConfig];
        }else{
            [self getUserDetails];
        }
    }
    else{
        [self.view makeToast:@"Please Check Your Internet"];
    }
    
}



-(void)getXmlConfig{
    NSString *url;
    if([[userDefault objectForKey:@"isIpv6"] isEqualToString:@"true"]){
         url = configTestURL;
    }else{
         url = configURL;
    }
    
        Boolean network = [NetworkManager getNetworkStatus];
        if(network){
            GetDataController *controller = [[GetDataController alloc] init];
                controller.delegate = self;
                [controller getFromUrl:url withData:nil withTag:200];
        }
        else{
            [self.view makeToast:@"Please Check Your Internet"];
        }
}


-(void) getFinishedWithData:(id)responseObject withTag:(NSUInteger)tag{

    NSLog(@"%@",responseObject);
    
    if(tag == 200){
        
        if (![responseObject isEqualToString:@"99"])
        {
            NSData	*b64DecData = [Base64 decode:responseObject];
            NSString *msg = [[NSString alloc] initWithData:b64DecData encoding:NSASCIIStringEncoding];
            XMLParser *xmlParser = [[XMLParser alloc] init];
            xmlParser.delegate = self;
            [xmlParser parseData:msg];
        }
        else{
            [self.view makeToast:@"Incorrect Operator Code"];
        }
    }
    
}

-(void) getFinishedWithError:(NSError *)error{

    NSLog(@"%@",error);
}

-(void) xmlParserFinishedParsingConfig{

    NSUserDefaults *newUser = [NSUserDefaults standardUserDefaults];

    NSString *sipIP = [newUser objectForKey:data_sip_ip];
    NSString *username;
    
    if([[userDefault objectForKey:@"isIpv6"] isEqualToString:@"true"]){
        username = _tfUsername.text;
    }else{
        username = [NSString stringWithFormat:@"%@%@",countryCode[position],_tfUsername.text];
    }
    
    
    if([username containsString:@"+"]){
        username = [username stringByReplacingOccurrencesOfString:@"+" withString:@""];
    }
    
    [userDefault removeObjectForKey:data_opcode];
    [userDefault removeObjectForKey:data_user_name];
    [userDefault removeObjectForKey:data_password];
    [userDefault setObject:_tfOppcode.text forKey:data_opcode];
    [userDefault setObject:username forKey:data_user_name];
    [userDefault setObject:_tfPassword.text forKey:data_password];
    
    if(sipIP != nil && ![sipIP isEqualToString:@""]){
        
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        
        [userDefault setObject:@"true" forKey:data_isAccountCreated];
        TabViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tabview"];
        viewController.isFirstLaunch = TRUE;
        [self.navigationController pushViewController:viewController animated:YES];
        
    }

}

- (void)updateLogInButton {
    BOOL textFieldsNonEmpty = self.tfUsername.text.length>2
     && self.tfPassword.text.length > 2;
    //BOOL isValidEmail = [TextFieldUtils NSStringIsValidEmail:self.tfEmail.text];
    self.btnSignUp.enabled = textFieldsNonEmpty;
    
    if(textFieldsNonEmpty){
        self.btnSignUp.customTextColor = [UIColor whiteColor];
    }else{
        self.btnSignUp.customTextColor = [UIColor grayColor];
    }
}

-(void)dismiskeyboard{
    [self.tfOppcode resignFirstResponder];
    [self.tfUsername resignFirstResponder];
    [self.tfPassword resignFirstResponder];
    [self.pickerContainer removeFromSuperview];
    
    if(![[userDefault objectForKey:@"isIpv6"] isEqualToString:@"true"]){
        [self setCountryCode:position];
    }
    
    if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}


-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:self.tfOppcode] || [sender isEqual:self.tfUsername] || [sender isEqual:self.tfPassword])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


//MARK: MOR API

-(void)getUserDetails{
    
    NSString *username = [NSString stringWithFormat:@"%@%@",countryCode[position],_tfUsername.text];
    if([username containsString:@"+"]){
        username = [username stringByReplacingOccurrencesOfString:@"+" withString:@""];
    }
    
    NSString *url = @"http://sip.shellvoice.com/billing/api/user_details_get";
    PostDataController *controller = [[PostDataController alloc] init];
    controller.delegate = self;
    NSDictionary *param = @{@"u": @"appconnect",
                            @"username": username
                            };
    [controller postwithURL:url withData:param withTag:102];
}

-(void)postFinishedWithData:(id)responseObject withTag:(NSUInteger)tag{
    
    if(tag == 102){
        NSLog(@"Response: %@", responseObject);
        XMLDictionaryParser *parser = [[XMLDictionaryParser alloc] init];
        NSDictionary *dic = [parser dictionaryWithParser:responseObject];
        
        NSString *err = [dic valueForKey:@"__text"];
        if(err != nil){
            [self.view makeToast:err];
            return;
        }
        
        NSString *userId = [dic valueForKey:@"userid"];
        NSDictionary *dic2 = [dic valueForKey:@"details"];
        NSDictionary *dic3 = [dic2 valueForKey:@"registration"];
        
        NSString *userEmail = [dic3 valueForKey:@"reg_email"];
        
        [userDefault setObject:userId forKey:data_morUserId];
        [userDefault setObject:userEmail forKey:data_regEmail];
        [self getXmlConfig];
    }
}

-(void)postFinishedWithError:(NSError *)error{
    NSLog(@"Error: %@", error);
}

- (IBAction)pickerDonePressed:(id)sender {
    [self setCountryCode:position];
}
@end
