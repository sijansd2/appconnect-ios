
#import "SignUpViewController.h"
#import "ViewExtension.h"
#import "TextFieldUtils.h"
#import "CountryDataSource.h"
#import "UIViewController+MJPopupViewController.h"
#import "LoginManager.h"
#import "Constans.h"
#import "MyEncryption.h"
#import "OtpViewController.h"
#import "UIView+Toast.h"
#import "MainPageViewController.h"
#import "SVProgressHUD.h"
#import "Base64.h"
#import "TabViewController.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "NetworkManager.h"
#import "MorApi.h"
#import "XMLDictionary.h"
#import "WelcomeViewController.h"
#import "MorCountry.h"
#import "LogInViewController.h"

#define kOFFSET_FOR_KEYBOARD 80.0

@interface SignUpViewController (){

    NSUserDefaults *userDefault;
    NSArray * countryName;
    NSArray * countryCode;
    NSArray * countryId;
    int position;
}

@property (strong, nonatomic) NSArray *countryObjects;

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    userDefault =[NSUserDefaults standardUserDefaults];
    position = 0;
    [self updateView];
    
    //Tap gesture enable
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiskeyboard)];
    [self.view addGestureRecognizer:tap];
    
    MorCountry *morCountry = [[MorCountry alloc] init];
    countryName = morCountry.countryName;
    countryCode = morCountry.countryCode;
    countryId = morCountry.countryId;
    
    self.picker.dataSource = self;
    self.picker.delegate = self;
}

-(void) viewWillAppear:(BOOL)animated{

    if([userDefault objectForKey:data_isAccountCreated] != nil &&
       [[userDefault objectForKey:data_isAccountCreated] isEqualToString:@"true"]){
        
        TabViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tabview"];
        AppDelegate *app = (AppDelegate *) [[UIApplication sharedApplication] delegate];
        [app.window setRootViewController:viewController];
        [viewController setSelectedIndex:1];
        
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//MARK: PickerView
// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return countryName.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return countryName[row];
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = countryName[row];
    NSAttributedString *attString =
    [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"%ld", (long)row);
    position = (int)row;
}


-(BOOL) textFieldShouldBeginEditing:(UITextField *)textField{
    
    if(textField == self.tfOppcode){
        [self showPickerView];
        return NO;
    }
    return YES;
}

-(void) updateView{
    
    [TextFieldUtils addImageLeftSideTextView:self.tfOppcode imageName:@"flag.png"];
    [TextFieldUtils addImageRightSideTextView:self.tfOppcode imageName:@"arrow_right.png"];
    [TextFieldUtils addImageLeftSideTextView:self.tfUsername imageName:@"phone.png"];
    [TextFieldUtils addImageLeftSideTextView:self.tfPassword imageName:@"password.png"];
    [TextFieldUtils addImageLeftSideTextView:self.tfEmail imageName:@"mail.png"];
    
    self.tfOppcode.delegate=self;
    self.tfUsername.delegate=self;
    self.tfPassword.delegate=self;
    self.tfEmail.delegate=self;
    
    self.tfOppcode.textColor = [UIColor whiteColor];
    self.tfPassword.textColor = [UIColor whiteColor];
    self.tfUsername.textColor = [UIColor whiteColor];
    self.tfEmail.textColor = [UIColor whiteColor];
    
    [self.tfOppcode addTarget:self action:@selector(updateLogInButton) forControlEvents:UIControlEventEditingChanged];
    [self.tfUsername addTarget:self action:@selector(updateLogInButton) forControlEvents:UIControlEventEditingChanged];
    [self.tfPassword addTarget:self action:@selector(updateLogInButton) forControlEvents:UIControlEventEditingChanged];
    [self.tfEmail addTarget:self action:@selector(updateLogInButton) forControlEvents:UIControlEventEditingChanged];
    
    [self updateLogInButton];
}


-(void)setCountryCode:(int) position{
    
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    CGSize stringBoundingBox = [countryCode[position] sizeWithFont:font];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 35+stringBoundingBox.width, 40)];
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(10, 8, 24, 24)];
    imageview.image = [UIImage imageNamed:@"phone.png"];
    imageview.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:imageview];
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(35, 0, stringBoundingBox.width, 39)];
    label.text = countryCode[position];
    label.textColor=[UIColor whiteColor];
    label.font = font;
    [view addSubview:label];
    self.tfUsername.leftViewMode = UITextFieldViewModeAlways;
    self.tfUsername.leftView = view;
    [self.tfUsername clipsToBounds];
    
    self.tfOppcode.text = countryName[position];
}

- (IBAction)btnSignUpPressed:(id)sender {
    [self.view endEditing:YES];

    [self registerUser];
}


-(void)showPickerView{
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.pickerContainer.bounds];
    self.pickerContainer.layer.masksToBounds = NO;
    self.pickerContainer.layer.shadowColor = [UIColor blackColor].CGColor;
    self.pickerContainer.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    self.pickerContainer.layer.shadowOpacity = 0.5f;
    self.pickerContainer.layer.shadowPath = shadowPath.CGPath;
    self.pickerContainer.center = self.view.center;
    [self.view addSubview:self.pickerContainer];
}



- (void)updateLogInButton {
    BOOL textFieldsNonEmpty = self.tfOppcode.text.length > 0 && self.tfUsername.text.length>2
     && self.tfPassword.text.length > 2;
    BOOL isValidEmail = [TextFieldUtils NSStringIsValidEmail:self.tfEmail.text];
    self.btnSignUp.enabled = textFieldsNonEmpty && isValidEmail;
    
    if(textFieldsNonEmpty && isValidEmail){
        self.btnSignUp.customTextColor = [UIColor whiteColor];
    }else{
        self.btnSignUp.customTextColor = [UIColor grayColor];
    }
}

-(void)dismiskeyboard{
    [self.tfOppcode resignFirstResponder];
    [self.tfUsername resignFirstResponder];
    [self.tfPassword resignFirstResponder];
    [self.tfEmail resignFirstResponder];
    [self setCountryCode:position];
    [self.pickerContainer removeFromSuperview];
    if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}


-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:self.tfOppcode] || [sender isEqual:self.tfUsername] || [sender isEqual:self.tfPassword] || [sender isEqual:self.tfEmail])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


//MARK: MOR API
-(void)registerUser{
    
    NSString *url = @"http://sip.shellvoice.com/billing/api/user_register";
    PostDataController *controller = [[PostDataController alloc] init];
    controller.delegate = self;
    NSString *cc = countryCode[position];
    NSString *username = [NSString stringWithFormat:@"%@%@",countryCode[position],_tfUsername.text];
    if([username containsString:@"+"]){
        username = [username stringByReplacingOccurrencesOfString:@"+" withString:@""];
    }
    NSDictionary *param = @{@"id": @"cpmdjfywk6",
                            @"username": username,
                            @"password": self.tfPassword.text,
                            @"password2": self.tfPassword.text,
                            @"first_name": cc,
                            @"last_name": self.tfUsername.text,
                            @"country_id": countryId[position],
                            @"email": self.tfEmail.text,
                            @"device_type": @"SIP"
                            };
    [controller postwithURL:url withData:param withTag:101];
    
}


-(void)deviceUpdate:(NSString*)deviceId{
    
    NSString *url = @"http://sip.shellvoice.com/billing/api/device_update?";
    PostDataController *controller = [[PostDataController alloc] init];
    controller.delegate = self;
    NSString *cc = countryId[position];
    NSString *userName = [NSString stringWithFormat:@"%@%@",cc,self.tfUsername.text];
    NSDictionary *param = @{@"u": @"admin",
                            @"device": deviceId,
                            @"extension": userName
                            };
    [controller postwithURL:url withData:param withTag:102];
}


-(void)postFinishedWithData:(id)responseObject withTag:(NSUInteger)tag{
    
    if(tag == 101){
        XMLDictionaryParser *parser = [[XMLDictionaryParser alloc] init];
        NSDictionary *dic = [parser dictionaryWithParser:responseObject];
        NSString *error = [dic valueForKeyPath:@"status.error"];
        
        if(error != nil){
            NSLog(@"MyDic Error--%@",error);
            [self showVoucherAlert:@"Server Response" messageBody:error withTag:0];
            return;
        }
        
        NSString *status = [dic valueForKeyPath:@"status.success"];
        NSString *deviceId = [dic valueForKeyPath:@"user_device_settings.device_id"];
        NSLog(@"Device ID:--%@",deviceId);
        if(status != nil){
            [self deviceUpdate:deviceId];
            [self showVoucherAlert:@"Success" messageBody:status withTag:3];
            
        }
    }
    
    else if(tag == 102){
        XMLDictionaryParser *parser = [[XMLDictionaryParser alloc] init];
        NSDictionary *dic = [parser dictionaryWithParser:responseObject];
        NSLog(@"Device update status: %@",dic);
    }

}

-(void)postFinishedWithError:(NSError *)error{
    NSLog(@"Error: %@", error);
}


//MARK: Alert
-(void) showVoucherAlert:(NSString*)title messageBody:(NSString*)msg withTag:(int)tag{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Ok"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    if(tag == 3){
//                                        LogInViewController * vc = [[LogInViewController alloc] init];
//                                        [self presentViewController:vc animated:YES completion:nil];
                                        [self.navigationController popViewControllerAnimated:YES];
                                    }
                                    
                                }];
    
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}


- (IBAction)pickerDonePressed:(id)sender {
    [self setCountryCode:position];
    [self.pickerContainer removeFromSuperview];
}
@end
