//
//  MainPageViewController.h
//  ShasthoNet
//
//  Created by Riaz on 6/22/16.
//  Copyright © 2016 Riaz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContentViewController.h"
#import "WhiteBorderedButton.h"

@interface MainPageViewController : UIViewController<UIPageViewControllerDataSource,UIPageViewControllerDelegate,UIScrollViewDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet WhiteBorderedButton *btnLogin;
@property (weak, nonatomic) IBOutlet WhiteBorderedButton *btnSignUp;

@property (nonatomic,assign) BOOL shouldBounce;
@property (nonatomic,assign) CGFloat lastPosition;
@property (nonatomic,assign) NSUInteger currentIndex;
@property (nonatomic,assign) NSUInteger nextIndex;

@end
