//
//  CallViewController.h
//  SymlexPro
//
//  Created by admin on 9/19/17.
//  Copyright © 2017 Kolpolok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CircularedImageView.h"
#import "MyApp.h"
#import <GoogleMobileAds/GADBannerView.h>

@interface CallViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>{
    
    IBOutlet UICollectionView *myCollectionView;
   
}

@property(nonatomic) NSString *phoneNumber;
@property(nonatomic) NSString *callType;
- (IBAction)endPressed:(id)sender;
- (IBAction)speakerPressed:(id)sender;
- (IBAction)holdPressed:(id)sender;
- (IBAction)mutePressed:(id)sender;
- (IBAction)keyPadPressed:(id)sender;
-(void) initWithCallId:(pjsua_call_id)callid;
-(void) initWithCallId:(pjsua_call_id)callid withString:(NSString *) callIDStrx;

@property (weak, nonatomic) IBOutlet UILabel *lbDtmf;

@property (weak, nonatomic) IBOutlet UIView *dtmfView;
- (IBAction)closeDtmf:(id)sender;
@property (weak, nonatomic) IBOutlet CircularedImageView *calleeImage;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *duration;
@property (weak, nonatomic) IBOutlet UIImageView *btnAccept;
@property (weak, nonatomic) IBOutlet UIView *acceptContainer;
@property (weak, nonatomic) IBOutlet UIView *endCallView;
@property (weak, nonatomic) IBOutlet GADBannerView *adview;

@property (strong, nonatomic) IBOutlet UIImageView *ivSource1, *ivDestination2, *tempIV;

@end
