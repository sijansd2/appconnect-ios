//
//  GetDataController.h
//  helloworld
//
//  Created by Riaz on 6/2/16.
//  Copyright © 2016 Riaz. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol  GetDataControllerDelegate;

@interface GetDataController : NSObject

-(void) initWithURL:(NSString*) pageurl withData:(NSDictionary *)data;

-(void) postDataAsGet:(NSString*) pageurl withData:(NSDictionary *)data;
-(void) getFromUrl:(NSString*) pageurl withData:(NSDictionary *)data withTag:(NSUInteger)tag;

@property (weak, nonatomic) id <GetDataControllerDelegate>delegate;

@end

@protocol GetDataControllerDelegate<NSObject>
@optional

- (void) getFinishedWithData :(id)responseObject;
-(void) getFinishedWithError:(NSError *)error;

-(void) postDataAsGetFinished:(id)responseObject;
- (void) getFinishedWithData :(id)responseObject withTag:(NSUInteger)tag;
@end
